import React from 'react';
import './form.css';

  class Form extends React.Component {
    state = {
      region: 'Ленинградская область',
      year: 2019,
      count: 2019,
      success: false,
      disabled: false
    }

    sendData = () => {
      if (!this.state.disabled) {
        fetch(`http://cyf.c-coin.org/api.php?type=insert&data=criminal&region_name=${this.state.region}&year=${this.state.year}&value=${this.state.count}`)
          .then(response => {
            this.setState({
              success: true,
              disabled: true
            })
            setTimeout(() => {
              this.setState({
                success: false,
                disabled: false
              })
            }, 3000);
            console.log(response)
          })
          .catch((error) => console.error('ERRROR', error));
      }
    }

    render() {
      return (
        <div className="form__wrap">
          <div className="form__title">Личные данные</div>
          <div className="form__fields">
            <div className="form__fields__group">
              <div className="form__fields__group__label">Имя</div>
              <input className="form__fields__group__control" defaultValue="Анатолий"></input>
            </div>
            <div className="form__fields__group">
              <div className="form__fields__group__label">Фамилия</div>
              <input className="form__fields__group__control" defaultValue="Сергиенко"></input>
            </div>
            <div className="form__fields__group">
              <div className="form__fields__group__label">Дата рождения</div>
              <input className="form__fields__group__control" defaultValue="10.02.1990"></input>
            </div>
          </div>
        </div>
      );
    }
  }

export default Form;