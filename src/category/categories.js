export const categories = [
  {
    id: 1,
    name: 'ДТП',
    api: 'http://cyf.c-coin.org/api.php?type=accident',
    premium: false,
  },
  {
    id: 2,
    name: 'Преступления',
    api: 'http://cyf.c-coin.org/api.php?type=crime',
    premium: false,
  },
  {
    id: 3,
    name: 'Демография',
    api: 'http://cyf.c-coin.org/api.php?type=demography',
    premium: false,
  },
  {
    id: 4,
    name: 'Экология',
    api: 'http://cyf.c-coin.org/api.php?type=eco',
    premium: false,
  },
  {
    id: 5,
    name: 'Заработаная плата',
    api: 'http://cyf.c-coin.org/api.php?type=salary',
    premium: true,
  },
  {
    id: 6,
    name: 'Стоимость жилья',
    api: 'http://cyf.c-coin.org/api.php?type=real-estate',
    premium: true,
  },
  {
    id: 7,
    name: 'ТОП 5 професий',
    api: 'http://cyf.c-coin.org/api.php?type=hh',
    premium: true,
  },
];