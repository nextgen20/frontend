// import Iframe from 'react-iframe';
import React, { Component } from 'react'

import init from './shared/init';
import legendImage from './shared/legend.png';
import legendRightImage from './shared/legend-right.png';
import { DataMapService } from './shared/DataMapService';

import Frame, { FrameContextConsumer } from 'react-frame-component';

import './Map.css'

export class Map extends Component {
  render() {
    const { category } = this.props;
    const dataMapService = new DataMapService({ category });

    return (
      <div className="map__container">
        <div className="map__label">
          Выберите интересующий Вас регион для более подробной информации 
        </div>

        <Frame
          style={{ width: '100%', height: '100%', border: 'none' }}
          initialContent={`<!DOCTYPE html> <html lang="en"> <head> <script>${'' + init}</script> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge"> <meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width"> <meta name="mobile-web-app-capable" content="yes"> <meta name="apple-mobile-web-app-capable" content="yes"> <link rel="stylesheet" href="/map/leaflet.css"> <link rel="stylesheet" href="/map/qgis2web.css"> <link rel="stylesheet" href="/map/map.css"> <style> html, body, #map { width: 100%; height: 100%; padding: 0; margin: 0; /* background-color: #bcbcbc; */ } </style> <title> Регионы России - генерализованные векторные данные </title> </head> <body> <div id="map" class="leaflet-container leaflet-touch leaflet-fade-anim leaflet-grab leaflet-touch-drag leaflet-touch-zoom" style="position: relative;" tabindex="0"> </div> <script src="/map/qgis2web_expressions.js"></script> <script src="/map/leaflet.js"></script> <script src="/map/leaflet.rotatedMarker.js"></script> <script src="/map/leaflet.pattern.js"></script> <script src="/map/leaflet-hash.js"></script> <script src="/map/Autolinker.min.js"></script> <script src="/map/rbush.min.js"></script> <script src="/map/labelgun.min.js"></script> <script src="/map/labels.js"></script> <script src="/map/source_0.js"></script> </body> </html>`}
        >
          <FrameContextConsumer>
            {
              // Callback is invoked with iframe's window and document instances
              ({document, window}) => {
                document.addEventListener("DOMContentLoaded", () => {
                  dataMapService
                    .fetchCategorySource()

                      // Успешно загрузили данные с сервера
                      .then((response) => {
                        // и теперь закрашиваем регоны
                        dataMapService.fillResponseColors();

                        // подготавилваем фрем для отрисовки
                        dataMapService.prepareMapFrame(window);

                        window.init();
                      });
                });

              }
            }
          </FrameContextConsumer>
        </Frame>

        <div className="map__legend">
          <img src={legendImage} alt='Уровень экологии' />
          <img src={legendRightImage} alt='Условное обозначение' />
        </div>
      </div>
    );
  }
}
