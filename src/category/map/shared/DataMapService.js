import { getRegionsData } from './getSources';

export class DataMapService {
  frame = null;
  category = null;
  regionsData = null;
  categoryResponseData = null;

  constructor({ category }) {
    this.category = category;
    this.regionsData = getRegionsData();
  }

  prepareMapFrame(frameWindow) {
    try {
      // Передаем фрейму функцию, которая ищет регион
      frameWindow.getSources = getSources.bind({data: this.regionsData});
          
      // Передаем фрейму функцию, которая подсвечивает регион
      frameWindow.highlightFeature = highlightFeature;
      frameWindow.onEachFeature = onEachFeature.bind(frameWindow);
    } catch (error) {
      console.error('Ошибка во время обработки фрейма', { error });
    }
  }

  /**
   * Метод перебирает локальные регионы и если он содерижит цвет от сервера
   * то перекрашивает в необходимый
   */
  fillResponseColors() {
    try {
      if (!this.regionsData && !Array.isArray(this.regionsData)) {
        throw new Error('не загружены локальные регионы');
      }

      this.regionsData.forEach((region) => {
        const responseData = region.responseData;

        if (responseData && responseData.color) {
          region.fillColor = this.getGroupFillColor(+responseData.color);
        }
      });
    } catch (error) {
      console.log('Ошибка во время раскрашивания регионов', { error });
    }
  }

  /**
   * Загружает данные региона 
   * @param {string} apiUrl Необязательный параметр запроса, по-умолчанию берет
   *                        данные из category переданного в конструктор
   * @returns {Promise} Возвращает с результатом запроса
   */
  fetchCategorySource(apiUrl) {
    try {
      if (typeof this.category === 'undefined') {
        throw new Error('Не передан парметр category в DataMapService');
      }

      if (typeof this.category.api === 'undefined' || !this.category.api) {
        throw new Error('Не передан параметр category.api в DataMapService');
      }

      const preapreResponseData = (response) => {
        if (response.hasOwnProperty('data')) {
          this.categoryResponseData = response.data;

          // Распределяем результат с сервара в локальных регионах
          if (Array.isArray(response.data)) {
            response.data.forEach((responseRegion, responseIndex) => {
              if (responseRegion && responseRegion['region_name']) {
                const responseRegionName = responseRegion['region_name'];
                const localRegion = this.regionsData.find((el) => {
                  if (Array.isArray(el.names)) {
                    if (el.names.indexOf(responseRegionName) !== -1) {
                      return true;
                    }
                  }
      
                  return el.name.toLowerCase() === responseRegionName.toLowerCase();
                });

                if(localRegion) {
                  localRegion.responseData = responseRegion;

                  if (this.category) {
                    localRegion.responseData.categoryId = this.category.id;
                    localRegion.responseData.responseIndex = responseIndex;
                  }

                  if (response.hasOwnProperty('fields')) {
                    localRegion.responseData.fields = response.fields;
                  }
                }
              }
            });
          }
        }

        return response;
      }

      if (apiUrl) {
        return fetch(apiUrl)
          .then(response => response.json())
          .then(preapreResponseData)
          .catch((error) => console.error('Исключение во время запроса', error));
      }

      return fetch(this.category.api)
        .then(response => response.json())
        .then(preapreResponseData)
        .catch((error) => console.error('Исключение во время запроса', error));

    } catch (error) {
      console.error('Ошибка во время загрузки данных регионов', { error });
    }
  }

  /**
   * Возвращает цвет необхоимой световой категории для раскрашивания региона
   * @param {number} group Цвет категории региона (приходит от сервера)
   */
  getGroupFillColor(group) {
    switch(group) {
      case 1: return 'rgba(255, 92, 92, 1.0)';
      case 2: return 'rgba(255, 172, 96, 1.0)';
      case 3: return 'rgba(252, 255, 108, 1.0)';
      case 4: return 'rgba(125, 255, 92, 1.0)';
      default: return 'rgba(118, 252, 195, 1.0)';
    }
  }
}

/**
 * Функция возвращает объект с параметрами региона из локального хранилища
 * @param {*} feature Параметр передается из leaflet
 */
function getSources(feature) {
  return this.data.find((region) => {
    const name = String(feature.properties['name']);
    const names = region.names;

    const sInName = (region.name === String(feature.properties['name']));
    const sInNames = (Array.isArray(names) && (names.indexOf(name) !== -1));

    return sInNames || sInName;
  });
}

/**
 * Функция подсвечивание региона
 * @param {LeafLeat Event} event
 */
function highlightFeature(event) {
  let highlightLayer = event.target;

  let color;
  if (
    event
    && event.target
    && event.target.options
    && event.target.options.responseData
    && event.target.options.responseData.color
  ) {
    color = (function(responseColor) {
      switch(responseColor) {
        case 1: return 'rgba(255, 162, 165, 1.0)';
        case 2: return 'rgba(255, 212, 151, 1.0)';
        case 3: return 'rgba(252, 255, 181, 1.0)';
        case 4: return 'rgba(186, 255, 169, 1.0)';
        default: return 'rgba(160, 255, 215, 1.0)';
      }
    })(event.target.options.responseData.color);

  }

  if (event.target.feature.geometry.type === 'LineString') {
    highlightLayer.setStyle({ color: color });
  } else {
    highlightLayer.setStyle({
      fillColor: color,
      fillOpacity: 1
    });
  }
}

function onEachFeature(feature, layer) {
  layer.on({
    mouseout: function (e) {
      for (let i in e.target._eventParents) {
        e.target._eventParents[i].resetStyle(e.target);
      }
    },

    mouseover: highlightFeature,
  });

  let desk = '';
  let linkUrl = '';
  const descEl = document.createElement('div');
  if (
    layer
    && layer.options
    && layer.options.responseData
  ) {
    const responseData = layer.options.responseData;

    if (responseData.fields) {
      Object.keys(responseData.fields).forEach((field) => {
        if (responseData.hasOwnProperty(field)) {
          const fieldDesc = responseData.fields[field];
          const fieldValue = responseData[field];
  
          if (
            fieldDesc.label !== 'Регион'
            && fieldDesc.label !== 'Цвет'
            && fieldDesc.label !== 'Id региона'
          ) {
            const br = document.createElement('br');
            const text = `${fieldDesc.label}: ${fieldValue} ${fieldDesc.unit}`;
            const descText = document.createTextNode(text);

            descEl.appendChild(br);
            descEl.appendChild(descText);
          }
        }
      });
    }

    // if (responseData['region_name'] === 'Амурская область') {
      if (Object.prototype.hasOwnProperty.call(responseData, 'region_name')) {
        const regionName = responseData['region_name'];
        
        console.log('Response data', responseData);
      }

    // }

    if (responseData.categoryId && responseData.responseIndex) {
      const { categoryId, responseIndex } = responseData;
      linkUrl = `${window.parent.location.origin}/detail/${responseIndex}`;
      // linkUrl = `${window.parent.location.origin}/category/${categoryId}/detail/${responseIndex}`;
    }
  }

  // Link
  const link = document.createElement('a');
  const linkText = document.createTextNode('подробнее');
  link.appendChild(linkText);
  link.href = linkUrl;
  link.onclick = (e) => {
    e.preventDefault();
    if (!linkUrl) {
      
    }

    // window.parent.location = linkUrl

    console.log('IT IS WORk!', window.parent.location, linkUrl, layer, feature);
  }

  // Title
  const titleEl = document.createElement('strong');
  titleEl.style.color = '#1D65AD';
  const titleText = document.createTextNode(feature.properties['name']);
  titleEl.appendChild(titleText);

  // Block
  const block = document.createElement('div');

  if (block.classList) {
    block.classList.add('rada-popup');
  } else {
    block.className += ' rada-popup';
  }

  block.appendChild(titleEl);
  block.appendChild(descEl);
  // block.appendChild(link);


  layer.bindPopup(block, { maxHeight: 400 });
}