/* eslint-disable */
export default function init() {
  var map = L.map("map", {
    maxZoom: 6,
    minZoom: 4,
    zoomControl: true
  });

  var hash = new L.Hash(map);

  map.attributionControl.addAttribution("");

  var bounds_group = new L.featureGroup([]);

  function setBounds() {
    if (bounds_group.getLayers().length) {
      map.fitBounds(bounds_group.getBounds());
    }
    map.setMaxBounds(map.getBounds());
  }

  map.createPane("pane_source_0");
  map.getPane("pane_source_0").style.zIndex = 400;
  map.getPane("pane_source_0").style["mix-blend-mode"] = "normal";

  var layer_source_0 = new L.geoJson(json_source_0, {
    pane: "pane_source_0",
    style: getSources,
    onEachFeature: onEachFeature
  });

  bounds_group.addLayer(layer_source_0);
  map.addLayer(layer_source_0);
  setBounds();
}