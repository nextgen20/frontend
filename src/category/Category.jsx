import React, { Component } from 'react';
import { NavLink, Switch, Route } from 'react-router-dom';

import './Category.css';
import premiumImage from '../shared/images/premium.png';

import { Map } from './map/Map';
import { RegionList } from './regionList/RegionList';
import { categories } from './categories';
import { Detail } from './detail/Detail';

export class Category extends Component {
	state = {
		categories: [...categories]
	}

	render() {
		const { id, view } = this.props.match.params

		const currentCategory = (() => {
			if (id) {
				return this.state.categories.find(el => el.id === +id)
			} else {
				return this.state.categories[0]
			}
		})()

		const getNavRoute = category =>
			view ? `/category/${category.id}/${view}` : `/category/${category.id}/map`

		return (
			<div className="category">
				<div className="category__title">
					Сравнительные данные о регионах РФ
				</div>

				<div className="controls">
					{this.state.categories.map((category, index) => (
						<NavLink
							to={getNavRoute(category)}
							key={`nav-link-${index}`}
							style={category.premium ? { color: '#7D7D7D' } : {}}
							className="controls__button"
							activeClassName="controls__button--active"
						>
							{category.name}

							{category.premium ? (
								<img
									src={premiumImage}
									className="controls__button-image"
									alt="premium"
								/>
							) : null}
						</NavLink>
					))}
				</div>

				<div className="list__view-type">
					<NavLink
						activeClassName="list__view-type--active"
						to={`/category/${id}/map`}
					>
						Карта
					</NavLink>

					<NavLink
						style={{ marginLeft: 32 }}
						activeClassName="list__view-type--active"
						to={`/category/${id}/list`}
					>
						Список
					</NavLink>
				</div>

				<Switch>
					<Route
						path="/category/:id/map"
						component={() => <Map category={currentCategory} />}
					/>

					<Route
						path="/category/:id/list"
						component={() => <RegionList category={currentCategory} />} />

					{/* <Route
						path="/category/:id/detail/:regionId"
						component={Detail} /> */}
				</Switch>
			</div>
		)
	}
}
