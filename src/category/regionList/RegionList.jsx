import React, { Component } from "react";
import "./RegionList.css";

import { DataMapService } from "../map/shared/DataMapService";

const FILTERS_LIST = [
  { label: "По алфавиту", name: "alphabet" },
  { label: "По рейтингу", name: "rating" }
];

export class RegionList extends Component {
  state = {
    filter: "alphabet",
    data: []
  };

  componentWillMount() {
    const { category } = this.props;
    const dataMapService = new DataMapService({ category });

    dataMapService
      .fetchCategorySource()
      // Успешно загрузили данные с сервера
      .then(() => {
        // и теперь закрашиваем регоны
        dataMapService.fillResponseColors();

        this.setState({
          ...this.state,
          data: this.sortRegionsData(dataMapService.regionsData)
        });
      });
  }

  sortRegionsData(data, name) {
    const sortBy = name || this.state.filter;

    switch (sortBy) {
      case "alphabet": {
        function compareName(a, b) {
          if (a.name < b.name) {
            return -1;
          }
          if (a.name > b.name) {
            return 1;
          }

          return 0;
        }

        return data.sort(compareName);
      }

      case "rating": {
        function compareColor(regA, regB) {
          if (
            regA.responseData &&
            regA.responseData.color &&
            regB.responseData &&
            regB.responseData.color
          ) {
            return regA.responseData.color - regB.responseData.color;
          }

          return 0;
        }

        return data.sort(compareColor);
      }

      default:
        return data.sort(
          (a, b) => a.color.responseData.color > b.color.responseData.color
        );
    }
  }

  render() {
    return (
      <div className="region-list">
        <div className="filters">
          <div className="filters__title">Сортировка:</div>

          {FILTERS_LIST.map((filter, filterIndex) => (
            <span
              key={`filter-${filterIndex}`}
              onClick={this.changeFilter.bind(this, filter.name)}
              className={this.getFilterClass.call(this, filter.name)}
            >
              {filter.label}
            </span>
          ))}
        </div>

        <ul className="list">
          {this.state.data.map((item, index) => {
            return (
              <li className="list__item" key={`list-region-${index}`}>
                <div
                  className="list__item__icon"
                  style={{ backgroundColor: item.fillColor }}
                />
                {item.name}
              </li>
            );
          })}
        </ul>
      </div>
    );
  }

  changeFilter(name = "") {
    if (FILTERS_LIST.find(el => el.name === name)) {
      this.setState({
        ...this.state,
        filter: name,
        data: this.sortRegionsData(this.state.data, name)
      });
    }
  }

  getFilterClass(name = "") {
    if (name === this.state.filter) {
      return "filters__link filters__link--active";
    } else {
      return "filters__link";
    }
  }
}
