import React from 'react';

import mapImage from './home/shared/images/country.png';

export default function() {
  return (
    <div className="not_found__wrap">
      <img src={mapImage} alt="" className="map_image"/>
      <div className="not_found__content">
        <p className="not_found__title">
          404 Not Found
        </p>
      </div>
    </div>
  );
}