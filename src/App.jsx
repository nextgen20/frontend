import React from 'react';
import { BrowserRouter as Router, Route, Switch, NavLink } from 'react-router-dom';

// Images
import logo from './shared/images/logo.svg';
import userIcon from './shared/images/userIcon.png';
import premiumIcon from './shared/images/premium.png';

// Components
import { Home } from './home/Home';
import { Admin } from './admin/admin';
import { Category } from './category/Category';
import NotFound from './404';

import './App.css';
import { About } from './about/About';
import { Detail } from './category/detail/Detail';

class App extends React.Component {
  render() {
    return (
      <Router>
        <div className="app">
          <header className="app-header">
            <NavLink className="app-header__link" to="/">
              <img src={logo} alt="RADA Logo" />
            </NavLink>

            <NavLink className="app-header__link margin-left-auto" to="/about">
              <div className="app-header__link-content">
                <div className="app-header__link-label">
                  О проекте
                </div>
              </div>
            </NavLink>

            <NavLink className="app-header__link" to="/premium">
              <div className="app-header__link-content">
                <img
                  alt="premium"
                  src={premiumIcon}
                  className="app-header__link-icon"
                />

                <div className="app-header__link-label">
                  Премиум
                </div>
              </div>
            </NavLink>
            
            <NavLink className="app-header__link" to="/admin">
              <div className="app-header__link-content">
                <img
                  alt="premium"
                  src={userIcon}
                  className="app-header__link-icon"
                />

                <div className="app-header__link-label">
                  Сергиенко Анатолий
                </div>
              </div>
            </NavLink>
          </header>
          
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/detail/:regionId" component={Detail} />
            <Route path="/category/:id/:view" component={Category} />
            <Route path="/category/:id" component={Category} />
            <Route path="/admin" component={Admin} />
            <Route path="/about" component={About} />
            <Route component={NotFound} />
          </Switch>
          
        </div>
      </Router>
    );
  }
}

export default App;