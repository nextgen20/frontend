import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import './Home.css';

export class Home extends Component {
  render() {
    return (<div className="home">
      {/* <img src={countryImage} alt="Country"/> */}
      <div className="home__title">
        Актуальная информация о регионах Российской Федерации
      </div>

      <div className="home__subtitle">
        Найди место для развития себя и своей страны 
      </div>
      <NavLink to="/category/1/map" className="start-button">Начать работу</NavLink>
    </div>)
  }
}